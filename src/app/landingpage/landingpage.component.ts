/**
 * @Author: harsha
 * @Date:   2021-02-16T16:37:18+01:00
 * @Last modified by:   harsha
 * @Last modified time: 2021-02-16T20:27:48+01:00
 */

import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  NgZone,
  OnDestroy,
  OnInit,
  ViewChild,
} from "@angular/core";
import { Loader } from "@googlemaps/js-api-loader";
import { environment } from "../../environments/environment";
import * as faker from "faker";
import { Router } from "@angular/router";
import { AppConsts, SelectedBooking } from "../app.consts";
interface PlaceSlug {
  id: string;
  resultsSlug: google.maps.places.PlaceResult;
  marker: google.maps.Marker;
  amount: number;
}

@Component({
  selector: "app-home",
  templateUrl: "./landingpage.component.html",
  styleUrls: ["./landingpage.component.scss"],
})
export class LandingPageComponent implements OnInit, OnDestroy {
  @ViewChild("container") listingContainer: ElementRef;
  private loader: Loader;
  private map: google.maps.Map;
  private placesService: google.maps.places.PlacesService;
  private markerIcon = "/assets/images/svg/home-icon.svg";
  private markerIconActive = "/assets/images/svg/home-icon-active.svg";
  selectedItem: PlaceSlug;

  placeSlugs: PlaceSlug[] = [];

  constructor(
    private _cdr: ChangeDetectorRef,
    private _router: Router,
    private _zone: NgZone
  ) {
    const { googleApiKey } = environment;
    this.loader = new Loader({
      apiKey: googleApiKey,
      version: "weekly",
      libraries: ["places"],
    });
  }
  ngOnDestroy(): void {
    this.map.unbindAll();
  }

  userPositionTrack(): Promise<google.maps.LatLng> {
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(
        (resp) => {
          resolve(
            new google.maps.LatLng({
              lng: resp.coords.longitude,
              lat: resp.coords.latitude,
            })
          );
        },
        (err) => {
          resolve(null);
        }
      );
    });
  }

  /**
   * [selectedBookings description]
   * @return [description]
   */

  selectedBookings() {
    const data = localStorage.getItem(AppConsts.selectedBooking);
    if (data) {
      const item: SelectedBooking = JSON.parse(data);
      if (item) {
        this.selectHotel(item.id);
      } else if (this.placeSlugs.length > 0) {
        this.selectHotel(this.placeSlugs[0].id);
      }
    }
  }

  /**
   * [selectHotel description]
   * @param  placeid [description]
   * @return         [description]
   */

  selectHotel(placeid: string) {
    this.selectedItem = placeid
      ? this.placeSlugs.find((x) => x.id == placeid)
      : null;

    this.placeSlugs.forEach((m) => {
      const pos = m.marker.getPosition();
      if (
        this.selectedItem &&
        this.selectedItem.resultsSlug.geometry.location === pos
      ) {
        m.marker.setIcon(this.markerIconActive);
      } else {
        m.marker.setIcon(this.markerIcon);
      }
    });
    if (this.selectedItem) {
      this.map.panTo(this.selectedItem.resultsSlug.geometry.location);
    }
    setTimeout(() => {
      if (this.selectedItem) {
        const index = this.placeSlugs.findIndex((x) => x === this.selectedItem);
        const element = this.listingContainer.nativeElement.children[index];
        element.scrollIntoView();
      }
    }, 200);
  }

  /**
   * [confirmedBookings description]
   * @param  {id} [description]
   * @return      [description]
   */

  confirmedBookings({ id }) {
    const selectedItem = this.placeSlugs.find((x) => x.id === id);

    if (selectedItem) {
      const { photos, name, vicinity } = selectedItem.resultsSlug;
      const selectedBooking: SelectedBooking = {
        id: id,
        amount: selectedItem.amount,
        name: name,
        photo: photos && photos.length > 0 ? photos[0].getUrl({}) : "",
        description: vicinity,
      };
      localStorage.setItem(
        AppConsts.selectedBooking,
        JSON.stringify(selectedBooking)
      );
      this._zone.run(() => {
        this._router.navigate(["/checkout"]);
      });
    } else {
      alert("Something went wrong");
    }
  }

  createMarker(place: google.maps.places.PlaceResult): google.maps.Marker {
    if (!place.geometry || !place.geometry.location) return null;

    const marker = new google.maps.Marker({
      map: this.map,
      position: place.geometry.location,
      icon: this.markerIcon,
    });

    google.maps.event.addListener(marker, "click", () => {
      this.selectHotel(place.place_id);
    });
    return marker;
  }

  /**
   * [getCurrentlocation description]
   */

  getCurrentlocation(): void {
    const pos = this.map.getCenter();
    this.placesService.nearbySearch(
      {
        location: pos,
        radius: 1500,
        keyword: "hotel",
        types: ["establishment"],
      },
      (
        results: google.maps.places.PlaceResult[],
        status: google.maps.places.PlacesServiceStatus
      ) => {
        if (status === "OK") {
          this.stackPlaces(results);
          // this.showMarkers(results);
        }
        this._cdr.detectChanges();
      }
    );
  }

  /**
   * [stackPlaces description]
   * @param  results [description]
   * @return         [description]
   */

  stackPlaces(results: google.maps.places.PlaceResult[]) {
    const newResults = results.filter((x) => {
      const index = this.placeSlugs.findIndex((p) => p.id === x.place_id);
      return index < 0;
    });
    newResults.forEach((x) => {
      const newPlace: PlaceSlug = {
        id: x.place_id,
        amount: faker.random.number({ min: 50, max: 300 }),
        marker: this.createMarker(x),
        resultsSlug: x,
      };
      this.placeSlugs.push(newPlace);
    });
    if (!this.selectedItem) {
      this.selectedBookings();
    }
  }

  ngOnInit(): void {
    this.loader
      .load()
      .then(this.userPositionTrack)
      .then((userPos: google.maps.LatLng) => {
        if (userPos) {
          return userPos;
        } else {
          return new google.maps.LatLng(52.520008, 13.404954);
        }
      })
      .then((pos: google.maps.LatLng) => {
        this.map = new google.maps.Map(
          document.getElementById("mapBlock") as HTMLElement,
          {
            center: pos,
            zoom: 11,
            disableDefaultUI: true,
          }
        );
        this.placesService = new google.maps.places.PlacesService(this.map);
        this.getCurrentlocation();
        google.maps.event.addListener(this.map, "dragend", () => {
          this.getCurrentlocation();
        });
      });
  }
}
