/**
 * @Author: harsha
 * @Date:   2021-02-16T16:37:18+01:00
 * @Last modified by:   harsha
 * @Last modified time: 2021-02-16T20:27:04+01:00
 */

import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";

@Component({
  selector: "list-item",
  templateUrl: "./gridlist.component.html",
})
export class GridListComponent implements OnInit {
  @Input("item") resultsSlug: google.maps.places.PlaceResult;
  @Input("amount") amount: number;
  @Input("id") id: string;
  @Input("isSelected") isSelected: boolean;
  @Output() onBookSelect = new EventEmitter<google.maps.places.PlaceResult>();

  constructor() {}

  ngOnInit(): void {}

  /**
   * [descriptionData description]
   * @return [description]
   */
  get descriptionData(): string {
    const { vicinity } = this.resultsSlug;
    if (vicinity) {
      return vicinity;
    }
    return "-";
  }

  /**
   * [priceValue description]
   * @return [description]
   */

  get priceValue(): string {
    return `€ ${this.amount.toFixed(0)}`;
  }

  /**
   * [titleData description]
   * @return [description]
   */

  get titleData(): string {
    return this.resultsSlug.name;
  }

  /**
   * [imageData description]
   * @return [description]
   */

  get imageData(): string {
    const { photos } = this.resultsSlug;
    if (photos && photos.length > 0) {
      return photos[0].getUrl({ maxWidth: 80 });
    }
    return "";
  }
}
