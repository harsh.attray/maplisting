import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckoutListingComponent } from './checkout.component';

describe('CheckoutListingComponent', () => {
  let component: CheckoutListingComponent;
  let fixture: ComponentFixture<CheckoutListingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CheckoutListingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckoutListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
