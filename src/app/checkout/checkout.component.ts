/**
 * @Author: harsha
 * @Date:   2021-02-16T16:37:18+01:00
 * @Last modified by:   harsha
 * @Last modified time: 2021-02-16T20:29:46+01:00
 */

import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  OnInit,
} from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { AppConsts, SelectedBooking } from "../app.consts";

@Component({
  selector: "app-checkout",
  templateUrl: "./checkout.component.html",
  styleUrls: ["./checkout.component.scss"],
})
export class CheckoutListingComponent implements OnInit, AfterViewInit {
  item: SelectedBooking = null;
  saved: boolean = false;
  bookingForm: FormGroup;
  constructor(
    private _cdr: ChangeDetectorRef,
    private _router: Router,
    private formBuilder: FormBuilder
  ) {
    this.bookingForm = this.formBuilder.group({
      firstName: ["", Validators.required],
      lastName: ["", Validators.required],
      dates: ["", Validators.required],
      emailAddress: ["", [Validators.required, Validators.email]],
    });
  }
  ngAfterViewInit(): void {
    if (!this.item) {
      this._router.navigate(["/"]);
    }
  }

  ngOnInit(): void {
    const data = localStorage.getItem(AppConsts.selectedBooking);
    if (data) {
      this.item = JSON.parse(data);
    }
    this._cdr.detectChanges();
  }

  /**
   * [datesVal description]
   * @return [description]
   */

  get datesVal() {
    return this.bookingForm.get("dates");
  }

  /**
   * [saveData description]
   */

  saveData(): void {
    if (this.bookingForm.valid) {
      this.saved = true;
      localStorage.removeItem(AppConsts.selectedBooking);
    }
  }

  /**
   * [firstNameVal description]
   * @return [description]
   */

  get firstNameVal() {
    return this.bookingForm.get("firstName");
  }
  /**
   * [lastNameVal description]
   * @return [description]
   */
  get lastNameVal() {
    return this.bookingForm.get("lastName");
  }
  /**
   * [emailVal description]
   * @return [description]
   */
  get emailVal() {
    return this.bookingForm.get("emailAddress");
  }
  /**
   * [titleData description]
   * @return [description]
   */

  get titleData(): string {
    return this.item.name;
  }

  /**
   * [imageData description]
   * @return [description]
   */

  get imageData(): string {
    return this.item.photo;
  }

  /**
   * [descriptionData description]
   * @return [description]
   */

  get descriptionData(): string {
    return this.item.description;
  }
  /**
   * [priceData description]
   * @return [description]
   */

  get priceData(): string {
    return `€ ${this.item.amount.toFixed(0)}`;
  }
}
